import './casa.css'

function Casa(props){
   
    return(
        <div className="casa" onClick={()=> props.handleClick(props.id)}>
            {props.valor}
        </div>
    )
}

export default Casa