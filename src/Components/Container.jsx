import { useState } from "react";
import "./container.css";
import Casa from "./Casa.jsx";
import Jogador from "./Jogador.jsx";

import verificar from "../scripts/VerificarJogo";

function Container() {
  const [casas, setCasas] = useState([
    { id: 0, valor: "" },
    { id: 1, valor: "" },
    { id: 2, valor: "" },
    { id: 3, valor: "" },
    { id: 4, valor: "" },
    { id: 5, valor: "" },
    { id: 6, valor: "" },
    { id: 7, valor: "" },
    { id: 8, valor: "" },
  ]);
  const [jogador, setJogador] = useState(true);

  const handleClick = (casaId) => {
    const newCasas = casas.map((casa) => {
      if (casa.id === casaId) return { ...casa, valor: jogador ? "X" : "O" };
      return casa;
    });
    setJogador(!jogador);
    verificar(casas);
    setCasas(newCasas);
  };
  return (
    <div className="container">
      <Jogador jogador={jogador ? "X" : "O"} />
      <div className="casas">
        {casas.map((casa) => {
          return (
            <Casa
              key={casa.id}
              id={casa.id}
              valor={casa.valor}
              handleClick={handleClick}
            />
          );
        })}
      </div>
    </div>
  );
}

export default Container;
